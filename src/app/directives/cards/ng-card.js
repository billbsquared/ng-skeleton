function ngCard /* @ngInject */($compile) {

  let tpl = require('./ng-card.html');

  return {
    restrict: 'E',
    scope: {
      card: '='
    },
    link: ($scope, $ele) => {

      if ($scope.card.flex) {
        tpl = require('./ng-card-flex.html');
      }

      $scope.card.title.class = $scope.card.title.class || 'md-title';

      $ele.replaceWith($compile(tpl)($scope));

    }
  }

}
export default ngCard
