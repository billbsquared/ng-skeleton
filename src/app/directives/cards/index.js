import ngCard from './ng-card';

export default ngModule => {

  ngModule
    .directive('ngCard', ngCard)

}
