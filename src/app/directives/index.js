import cards from './cards';
import icons from './icons';

export default ngModule => {

  cards(ngModule);
  icons(ngModule);

}
