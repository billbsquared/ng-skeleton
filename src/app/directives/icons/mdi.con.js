function mdiCon /* @ngInject */($compile, $interpolate) {

  let tpl = `<md-icon md-svg-icon="mdi:{{ icon }}" ng-style="{color: color}" ng-class="iSize"></md-icon>`;

  return {
    restrict: 'E',
    scope: {
      color: '@?',
      size: '@?',
      icon: '@?'
    },
    link: ($scope, $ele) => {

      $scope.color = $scope.color || 'rgba(0,0,0,0.54)';

      $scope.iSize = $scope.size ? `icon-${$scope.size}` : `icon-24`;

      $scope.icon = $scope.icon || $interpolate($ele.text())($scope);

      $ele.replaceWith($compile(tpl)($scope));
    }
  }

}
export default mdiCon
