import camel from 'camelcase';
import decamelize from 'decamelize';
import path from 'path';

function pageFactory /* @ngInject */($window, $rootScope) {

  const v = $window.validator;
  const titles = $rootScope.site.titles;
  const characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

  function to(obj, expected = null) {
    let to = typeof obj;
    if (expected)
      return to == expected;
    return to;
  }

  // String functions
  function camelize(str, has_ext = false) {
    let ext = has_ext ? path.extname(str) : '';
    let basename = str.replace(ext, '');
    return camel(basename) + ext;
  }

  function deCamelize(str, separator) {
    return decamelize(str, separator);
  }

  function randomString(i = 8) {
    let str = '';
    while (str.length < i)
      str += characters[Math.round(Math.random() * (characters.length - 1))];
    return str;
  }

  function ucWords(str) {
    return String(str).replace(/^([a-z\u00E0-\u00FC])|\s+([a-z\u00E0-\u00FC])/g, $1 => $1.toUpperCase());
  }

  // Title manipulation
  function joinTitle(title, separator, prepend = false) {

    if (title) {
      let titleTo = to(title);

      switch (titleTo) {
        case 'string':
          title = title.trim();
          if (title == '') {
            return null;
          }
          break;
        case 'object':
          title = title.join(separator);
          break;
        default:
          return null;
      }
    } else {
      return null;
    }

    return prepend ? separator + title : title;
  }

  function toolbarTitle(title, separator = titles.toolbarSeparator) {
    title = joinTitle(title, separator);

    title = title || '';

    $rootScope.page.toolbarTitle = title;
  }

  function windowTitle(title, separator = titles.windowSeparator) {
    title = joinTitle(title, separator);

    title = title ? title + separator : '';

    $window.document.title = title + titles.windowTitle;
  }

  function appTitles(title) {
    this.toolbarTitle(title);
    this.windowTitle(title);
  }

  function identityType(identity) {
    let identity_type = 'username';

    if (v.isEmail(identity)) identity_type = 'email';
    if (v.isMobilePhone(identity, 'en-US')) identity_type = 'phone';

    return identity_type;
  }

  return {

    // String functions
    camelize: camelize,
    decamelize: deCamelize,
    randomString: randomString,
    ucWords: ucWords,

    // Titles
    toolbarTitle: toolbarTitle,
    windowTitle: windowTitle,
    appTitles: appTitles,

    // Misc
    identityType: identityType

  }

}
export default pageFactory
