import appSettings from '../app-settings';

function interceptor /* @ngInject */($q, $ss) {

  let apiHeaders = appSettings.api.headers;

  return {

    request: config => {
      config.headers = config.headers || {};

      Object.keys(apiHeaders).forEach(h => {
        if ($ss.retrieve(h)) config.headers[apiHeaders[h]] = $ss.retrieve(h);
      });

      return config;
    },

    requestError: rej => {
      return $q.reject(rej);
    },

    response: res => {
      return res;
    },

    responseError: rej => {
      return $q.reject(rej);
    }

  }

}
export default interceptor
