import pageFactory from './page';
import interceptor from './interceptor';

export default ngModule => {

  ngModule
    .factory('$page', pageFactory)
    .factory('authInterceptor', interceptor)

}
