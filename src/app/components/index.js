import home from './home';
import shared from './shared';

export default ngModule => {

  home(ngModule);
  shared(ngModule);

}
