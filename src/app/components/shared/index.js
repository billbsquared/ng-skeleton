import header from './header';
import footer from './footer';

export default ngModule => {

  ngModule
    .controller('headerCtrl', header)
    .controller('footerCtrl', footer)

}
