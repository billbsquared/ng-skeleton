import home from './home';

export default ngModule => {

  ngModule
    .config($stateProvider => {

      $stateProvider
        .state('app.home', {
          url: '/',
          views: {
            'main@': {
              controller: $rootScope => {
                $rootScope.rows = $rootScope.getRows(3);

                $rootScope._layout.row1.flex = {
                  gt_sm: 25,
                  gt_sm_offset: 0
                };
                $rootScope._layout.row2.flex = {
                  gt_sm: 50,
                  gt_sm_offset: 0
                };
                $rootScope._layout.row3.flex = {
                  gt_sm: 25,
                  gt_sm_offset: 0
                };
              },
              templateProvider: $templateCache => $templateCache.get('layouts/multi.row')
            },
            'row1@app.home': {
              templateProvider: $templateCache => $templateCache.get('misc/text')
            },
            'row2@app.home': {
              controller: home,
              controllerAs: 'home',
              templateProvider: $templateCache => $templateCache.get('home/main')
            },
            'row3@app.home': {
              templateProvider: $templateCache => $templateCache.get('misc/text')
            }
          }
        })

    })

}
