import appSettings from './app-settings';

function appRunblock /* @ngInject */($rootScope, $state, $mdDialog, $mdMedia, $ss, $geo, $log, $templateCache, $document, $location, $window, $anchorScroll) {

  // Force https on non-localhost
  if (appSettings.env !== 'dev')
    if ($location.protocol() !== 'https')
      $window.location.href = $location.absUrl().replace('http', 'https');

  // Settings
  let site = appSettings.site;
  $rootScope.site = site;
  $rootScope.titles = site.titles;
  $rootScope.page = {};

  // Layouts
  $rootScope._show = {
    header: true,
    footer: true
  };
  $rootScope._layout = {
    main: {
      layout: 'column',
      flex: 90,
      flex_offset: 5
    },
    row1: {
      layout: 'column',
      flex: {
        flex: 90,
        offset: 0,
        gt_sm: 90,
        gt_sm_offset: 5
      }
    },
    row2: {
      layout: 'column',
      flex: {
        flex: 90,
        offset: 0,
        gt_sm: 50,
        gt_sm_offset: 0
      }
    },
    row3: {
      layout: 'column',
      flex: {
        flex: 90,
        offset: 0,
        gt_sm: 33,
        gt_sm_offset: 0
      }
    }
  };
  $rootScope.getRows = n => {
    n = parseInt(n);
    let rows = [];
    for( let i = 0; i <= n; i++) {
      rows.push(`row${i}`);
    }
    return rows;
  };

  // Redirects
  $rootScope.goto = (state, params = {}, reload = false) => $state.go(state, params, {reload: reload});
  $rootScope.anchor = anchor => $location.hash() !== anchor ? $location.hash(anchor) : $anchorScroll();

  // Dialogs
  let customFullscreen = $mdMedia('xs') || $mdMedia('sm');

  $rootScope.dialogTpl = (tpl, ev, co = true, fs = true) => {

    let useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && customFullscreen;

    $mdDialog.show({
      controller: dialogCtrl,
      template: $templateCache.get(tpl),
      parent: angular.element($document.body),
      targetEvent: ev,
      clickOutsideToClose: co,
      fullscreen: useFullScreen && fs
    });

    $rootScope.$watch(() => {
      return $mdMedia('xs') || $mdMedia('sm')
    }, wantsFullScreen => {
      customFullscreen = (wantsFullScreen === true);
    });

  };

  $rootScope.dialogAccept = false;

  function dialogCtrl($scope, $mdDialog) {

    $scope.hideDialog = () => {
      $rootScope.dialogAccept = false;
      broadcastDialog();
      $mdDialog.hide();
    };

    $scope.acceptDialog = () => {
      $rootScope.dialogAccept = true;
      broadcastDialog();
      $mdDialog.hide();
    };

    function broadcastDialog(val = false) {
      $rootScope.$broadcast('dialogAccept', val);
    }

  }

  // Geolocation
  $rootScope.geo = {
    id: null,
    position: null
  };

  let loc_ts = $ss.retrieve('geo_timestamp');

  if (!loc_ts || loc_ts < moment().subtract(1, 'hour').format('x')) {
    $geo.currentPosition()
      .then(() => {
        $log.info($rootScope.geo);
      })
      .catch(err => $log.error(err));
  }

}
export default appRunblock
