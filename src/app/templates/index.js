const tpls = require('./templates.json');

function cache /* @ngInject */($templateCache) {

  tpls.map(f => $templateCache.put(f, require(`./${f}.html`)));

}
export default cache
