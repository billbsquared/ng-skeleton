const apiUrl = '';
const appUrl = '';

export default {
  env: 'dev',
  api: {
    url: apiUrl,
    headers: {},
    keys: {}
  },
  site: {
    apiUrl: apiUrl,
    appUrl: appUrl,
    store: {
      prefix: '',
      engine: ''
    },
    titles: {
      toolbarTitle: '',
      toolbarSeparator: ' - ',
      windowTitle: '',
      windowSeparator: ' | ',
    },
  }
}
