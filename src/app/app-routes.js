function appRoutes /* @ngInject */($stateProvider) {

  $stateProvider
    .state('app', {
      abstract: true,
      resolve: {},
      views: {
        'header@': {
          controller: 'headerCtrl',
          controllerAs: 'head',
          templateProvider: $templateCache => $templateCache.get('shared/header')
        },
        'main@': {
          templateProvider: $templateCache => $templateCache.get('misc/blank')
        },
        'footer@': {
          controller: 'footerCtrl',
          controllerAs: 'foot',
          templateProvider: $templateCache => $templateCache.get('shared/footer')
        }
      }
    })

}
export default appRoutes
