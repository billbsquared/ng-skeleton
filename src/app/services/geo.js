import appSettings from '../app-settings';

function geo /* @ngInject */($window, $rootScope, $log, $q, $http, $ss) {

  let apiUrl = appSettings.api.url;
  let $nav = $window.navigator;
  let $geo = $nav.geolocation;
  let $geoOptions = {
    enableHighAccuracy: true,
    maximumAge: 30000,
    timeout: 27000
  };

  let $geoError = 'Geolocation is not available.';

  function parseLatLng(coords) {
    coords = String(coords);
    coords = coords.slice(0, 1) == '-' ? coords.slice(0, 8) : coords.slice(0, 7);
    return parseFloat(coords);
  }

  function parseCoords(pos) {
    let coords = pos.coords;
    let lat = parseLatLng(coords.latitude);
    let lng = parseLatLng(coords.longitude);

    return {
      latitude: lat,
      longitude: lng,
      accuracy: coords.accuracy,
      speed: coords.speed,
      timestamp: pos.timestamp
    }
  }

  function getCurrent() {
    if (!$geo) return $geoError;

    return new Promise((resolve, reject) => {
      $geo.getCurrentPosition(
        pos => {
          let coords = parseCoords(pos);

          $rootScope.geo.position = coords;

          $ss.store('geo', coords);
          resolve(true);
        },
        err => {
          $log.error(err);
          reject($geoError);
        },
        $geoOptions
      )
    });
  }

  function watchPosition() {
    if (!$geo) return $geoError;

    return new Promise((resolve, reject) => {
      $geo.watchPosition(
        pos => {
          let coords = parseCoords(pos);

          $rootScope.geo.position = coords;

          $ss.store('geo', coords);
          resolve(coords);
        },
        err => {
          $log.error(err);
          reject($geoError);
        },
        $geoOptions
      );
    });
  }

  return {
    currentPosition: getCurrent,
    watchPosition: watchPosition
  }

}
export default geo
