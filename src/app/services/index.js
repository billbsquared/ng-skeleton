import geo from './geo';

export default ngModule => {

  ngModule
    .service('$geo', geo)

}
