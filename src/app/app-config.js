function appConfig /* @ngInject */($locationProvider, $urlRouterProvider, $mdIconProvider, $mdThemingProvider, $httpProvider) {

  $locationProvider.html5Mode(true);

  $urlRouterProvider.otherwise('/404');

  $mdIconProvider.iconSet('mdi', '../assets/icons/mdi.svg');

  let background = $mdThemingProvider.extendPalette('blue-grey', {
    'A100': '#F5F5F5'
  });

  $mdThemingProvider.definePalette('background', background);

  $mdThemingProvider
    .theme('default')
    .primaryPalette('grey', {default: '800'})
    .accentPalette('red', {default: '900'})
    .backgroundPalette('background');

}
export default appConfig
