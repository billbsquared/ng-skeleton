require('../assets/styles/app.less');

const ngModule = angular.module('ngSkeleton', [
  'ngAnimate',
  'ngAria',
  'ngMessages',
  'ngMaterial',
  'ui.router',
  'angulartics',
  require('angulartics-google-analytics'),
  'simpleStore'
]);

import appConfig from './app-config';
import appRoutes from './app-routes';
import appRunblock from './app-runblock';
import appTemplates from './templates';

import appCtrl from './app-ctrl';

import appComponents from './components';
import appDirectives from './directives';
import appFactories from './factories';
import appFilters from './filters';
import appServices from './services';

ngModule
  .config(appConfig)
  .config(appRoutes)
  .run(appTemplates)
  .run(appRunblock)
  .controller('AppCtrl', appCtrl);

['moment', 'validator'].forEach(c => ngModule.constant(c, window[c]));

appComponents(ngModule);
appDirectives(ngModule);
appFactories(ngModule);
appFilters(ngModule);
appServices(ngModule);
