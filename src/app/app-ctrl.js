class appCtrl {

  constructor /* @ngInject */($rootScope, $ss) {
    this.$rootScope = $rootScope;
    this.$ss = $ss;

    this.card = {
      title: {
        class: 'md-headline',
        text: 'Example',
        sub: 'Subhead'
      },
      tpl: 'misc/text',
      flex: 50
    };

    this.cards = [
      {
        title: {
          class: 'md-headline',
          text: 'Example',
          sub: 'Subhead'
        },
        tpl: 'misc/text',
        flex: 25
      },
      {
        title: {
          class: 'md-headline',
          text: 'Example',
          sub: 'Subhead'
        },
        tpl: 'misc/text',
        flex: 25
      }
    ];
  }

}
export default appCtrl
