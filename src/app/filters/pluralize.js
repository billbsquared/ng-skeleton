function pluralize /* @ngInject */() {

  return (input) => {
    if (input.slice(-1) == 's') {
      input += `'`;
    } else if (input.slice(-2) == 'ex') {
      input += 'es';
    } else {
      input += `'s`;
    }

    return input;
  }

}
export default pluralize
