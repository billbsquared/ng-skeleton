function rangeOne /* @ngInject */() {

  return (input, total) => {
    total = parseInt(total);

    for (let i = 1; i < total + 1; i++) {
      input.push(i);
    }

    return input;
  }

}
export default rangeOne
