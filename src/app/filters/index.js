import pluralize from './pluralize';
import range from './range';
import rangeOne from './rangeOne';

export default ngModule => {

  ngModule
    .filter('pluralize', pluralize)
    .filter('range', range)
    .filter('rangeOne', rangeOne)

}
