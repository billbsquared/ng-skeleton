function range /* @ngInject */() {

  return (input, total) => {
    total = parseInt(total);

    for (let i = 0; i < total; i++) {
      input.push(i);
    }

    return input;
  }

}
export default range
