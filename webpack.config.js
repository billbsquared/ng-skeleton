const path = require('path');
const webpack = require('webpack');

let srcBase = path.join(__dirname, 'src');
let distBase = path.join(__dirname, 'dist');

let config = env => {

  let port = env.port || 8080;

  let contentBase = env.dev ? srcBase : distBase;

  let ng = [
    'angular',
    'angular-animate',
    'angular-aria',
    'angular-messages',
    'angular-sanitize',
    'angular-material',
    'angular-ui-router',
  ];

  let utilities = [
    'lodash',
    '@nsfw/simple.store',
    'angulartics',
    'camelize',
    'decamelize'
  ];

  let entry = {
    app: [path.join(srcBase, 'app', 'app.js')]
  };

  if (env.all) {
    entry = {
      ng: ng,
      utilities: utilities
    };
  }

  let wp = {
    entry: entry,
    output: {
      path: contentBase,
      publicPath: '/',
      filename: '[name].bundle.js',
      sourceMapFilename: '[file].map',
    },

    node: {
      console: true
    },

    plugins: [
      new webpack.optimize.DedupePlugin()
    ],

    devtool: env.dev ? 'inline-source-map' : 'source-map',

    module: {
      loaders: [
        {test: /\.js$/, exclude: /node_modules/, loader: 'ng-annotate!babel-loader'},
        {test: /\.json$/, exclude: /node_modules/, loader: 'json'},
        {test: /\.html$/, exclude: /node_modules/, loader: 'raw'},
        {test: /\.less$/, exclude: /node_modules/, loader: 'style!css!less'},
        {test: /\.css$/, exclude: /node_modules/, loader: 'style!css'}
      ]
    }
  };

  if (env.dev) {
    wp.devServer = {
      historyApiFallback: true,
      hot: true,
      inline: true,
      port: port,
      contentBase: contentBase
    };
  }

  return wp;
};

module.exports = config({dev: true, port: 8080});
