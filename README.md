# Readme title

### Index
1. Requirements
 1. Environment
 2. Dependencies
2. Purpose
3. Installation
 1. package.json
 2. Startup

## Requirements

#### Environment

#### Dependencies
* [body-parser](https://npmjs.com/package/body-parser)
* [chokidar](https://npmjs.com/package/chokidar)
* [compression](https://npmjs.com/package/compression)
* [cors](https://npmjs.com/package/cors)
* [express](https://npmjs.com/package/express)
* [file-stream-rotator](https://npmjs.com/package/file-stream-rotator)
* [fs-extra](https://npmjs.com/package/fs-extra)
* [ip](https://npmjs.com/package/ip)
* [jsonwebtoken](https://npmjs.com/package/jsonwebtoken)
* [lodash](https://npmjs.com/package/lodash)
* [moment](https://npmjs.com/package/moment)
* [mongodb](https://npmjs.com/package/mongodb)
* [mongoose](https://npmjs.com/package/mongoose)
* [morgan](https://npmjs.com/package/morgan)
* [multer](https://npmjs.com/package/multer)
* [request](https://npmjs.com/package/request)
* [validator](https://npmjs.com/package/validator)
* [yargs](https://npmjs.com/package/yargs)

## Purpose
_Project purpose_

## Installation
_Installation instructions_

#### package.json
```javascript
{
    "scripts": {
        "test": "echo \"Error: no test specified\" && exit 1",
        "start": "nodemon",               // uses nodemon.json file for config
        "serve": "pm2 start process.json" // uses process.json
    }
}
```

#### Startup

```bash
> npm install
> npm start     # For development
> npm server    # For production
```
